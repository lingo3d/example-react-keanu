import "./App.css"
import { Model, OrbitCamera, usePreload, World } from "lingo3d-react"
import React, { useState } from "react"

const Game: React.FC<{ quality: "quality" | "balanced" }> = ({ quality }) => {
  return (
    <World defaultLight="studio" performance={quality} color="transparent">
      <Model src="keanu.glb" scale={5} metalnessFactor={5} roughnessFactor={0.5} boxVisible={false} />
      <OrbitCamera z={400} active autoRotate enableZoom enablePan enableDamping />
    </World>
  )
}

export const App = () => {
  const progress = usePreload(["keanu.glb"], "20.5mb")
  const [quality, setQuality] = useState<"quality" | "balanced" | undefined>(undefined)

  if (progress < 100)
    return (
      <div className="container">
        loading {Math.round(progress)}%
      </div>
    )

  if (!quality)
      return (
        <div className="container">
          <button style={{ marginRight: 10 }} onClick={() => setQuality("balanced")}>
            Speed
          </button>
          <button onClick={() => setQuality("quality")}>
            Quality
          </button>
        </div>
      )

  return (
    <>
      <div className="container bg" style={{ backgroundImage: "url(bg0.webp)" }} />
      <Game quality={quality} />
    </>
  )
}

export default App
